package engine;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;

public class Baza {

      public static Connection Conn() throws SQLException{
       Connection conn = DriverManager.getConnection("jdbc:mysql://127.1.0.0/","root","2222");
       return conn;
      }
      public static void prikazi() throws SQLException{
          Connection conn = Conn();
          DatabaseMetaData db_metadata = conn.getMetaData();
          ResultSet rs = db_metadata.getCatalogs();
          while(rs.next()){
              System.out.println("Database: "+rs.getString(1));
          }
          conn.close();
      }
      public static void createDatabase() throws SQLException{
          System.out.println("Unesite ime baze: ");
          String naziv = new Scanner(System.in).nextLine();
          Connection conn = Conn();
          PreparedStatement ps = conn.prepareStatement("create database "+naziv+";");
          ps.execute();
          conn.close();
      }
      public static void deleteDatabase() throws SQLException{
          System.out.println("Unesite ime baze: ");
          String naziv = new Scanner(System.in).nextLine();
          Connection conn = Conn();
          PreparedStatement ps = conn.prepareStatement("drop database "+naziv+";");
          ps.execute();
          conn.close();
      }
}
